//
// Created by dell on 29.12.2020.
//

#include "Server.h"

using namespace std;

Server::Server(int hostShort) {
    //Reset and initialization of network address
    bzero((char *) &serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET; //IPV4
    serverAddress.sin_addr.s_addr = INADDR_ANY; //LOCALHOST
    serverAddress.sin_port = htons(hostShort); // ENDIAN

    //Creating socket in domain AF_INET
    mySocket = socket(AF_INET, SOCK_STREAM, 0);
    if (mySocket == -1) {
        perror("Error creating socket.");
    }

    //Socket address assignment
    if (bind(mySocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1) {
        perror("Error binding socket address.");
    }

    //Preparing socket to accept connections from clients
    listen(mySocket, 20);
    socklen_t client1Len = sizeof(client1Address);
    socklen_t client2Len = sizeof(client2Address);

    //Accepting connections from clients
    newSocket1 = accept(mySocket, (struct sockaddr *) &client1Address, &client1Len);
    if (newSocket1 == -1) {
        perror("Error on accept socket 1.");
    }
    newSocket2 = accept(mySocket, (struct sockaddr *) &client1Address, &client2Len);
    if (newSocket2 == -1) {
        perror("Error on accept socket 2.");
    }

    //Game
    this->game();
}

void Server::game() {

    srand(time(nullptr));

    //Sending welcome
    string welcome = "Welcome!\n Enter nick: \n";

    char* messageToSent = strdup(welcome.c_str());
    this->sendMessage(messageToSent, newSocket1);
    this->sendMessage(messageToSent, newSocket2);
    free((char*)messageToSent);

    //Loading nicks
    string nick1 = this->readMessage(newSocket1);
    string nick2 = this->readMessage(newSocket2);

    //Sending order and grid size
    int gridSize = 3 + rand() % 3;
    string sizeAndOrder1 = "1 " + to_string(gridSize) + "\n";
    string sizeAndOrder2 = "2 " + to_string(gridSize) + "\n";

    messageToSent = strdup(sizeAndOrder1.c_str());
    this->sendMessage(messageToSent, newSocket1);
    free((char*)messageToSent);

    messageToSent = strdup(sizeAndOrder2.c_str());
    this->sendMessage(messageToSent, newSocket2);
    free((char*)messageToSent);

    //Game
    Game *game = new Game(gridSize, nick1, nick2);
    for (int i = 0; i < gridSize * gridSize; i++) {
        char *move;
        bool incorrectInput = true;
        string descMove = "Your move: \n  (Format -> \"number letter\" ) ";
        messageToSent = strdup(descMove.c_str());
        if ((i % 2) == 0) {
            this->sendMessage(messageToSent, newSocket1);
        } else {
            this->sendMessage(messageToSent, newSocket2);
        }
        free((char*)messageToSent);

        while (incorrectInput) {
            if ((i % 2) == 0) {
                move = this->readMessage(newSocket1);
            } else {
                move = this->readMessage(newSocket2);
            }

            string verifyStringFormat(move);
            if(verifyStringFormat.find_first_not_of("abcdeABCDE12345 \n") != string::npos) {
                incorrectInput = true;
            } else {
                if(isalpha(verifyStringFormat[2])>0 && isalpha(verifyStringFormat[0])==0) {
                    incorrectInput = game->processUserInput(i, this->getNumber(move), this->getLetter(move));
                } else {
                    incorrectInput = true;
                }
            }

            if (incorrectInput) {
                string incorrectMessage = "1";
                messageToSent = strdup(incorrectMessage.c_str());
                if ((i % 2) == 0) {
                    this->sendMessage(messageToSent, newSocket1);
                } else {
                    this->sendMessage(messageToSent, newSocket2);
                }
                free((char*)messageToSent);
            } else {
                string correctMessage = "0";
                messageToSent = strdup(correctMessage.c_str());
                if ((i % 2) == 0) {
                    this->sendMessage(messageToSent,newSocket1);
                    this->sendMessage(move, newSocket2);
                } else {
                    this->sendMessage(messageToSent,newSocket2);
                    this->sendMessage(move, newSocket1);
                }
                free((char*)messageToSent);

                messageToSent = game->getScore();
                this->sendMessage(messageToSent, newSocket1);
                free(messageToSent);

                messageToSent = game->getScore();
                this->sendMessage(messageToSent, newSocket2);
                free(messageToSent);
            }
        }
    }

    //Evaluation
    messageToSent = game->gameOver();
    this->sendMessage(messageToSent, newSocket1);
    free((char*)messageToSent);

    messageToSent = game->gameOver();
    this->sendMessage(messageToSent, newSocket2);
    free((char*)messageToSent);
    delete game;
}

void Server::sendMessage(char *msg, int who) {
    n = write(who, msg, strlen(msg) + 1);
    if (n < 0) {
        perror("Error reading from socket.");
    }
}

char *Server::readMessage(int who) {
    bzero(buffer, 256);
    n = read(who, buffer, 255);
    if (n < 0) {
        perror("Error writing to socket.");
    }
    return buffer;
}

int Server::getNumber(string text) {
    string delimiter = " ";
    return stoi(text.substr(0, text.find(delimiter)));
}

char Server::getLetter(string text) {
    string delimiter = " ";
    string stringLetter = text.substr(text.find(delimiter) + 1, text.length());
    return stringLetter[0];
}