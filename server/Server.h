//
// Created by dell on 29.12.2020.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <ctime>
#include <iostream>
#include "../game/Game.h"


class Server {
private:
    struct sockaddr_in serverAddress, client1Address, client2Address;
    int mySocket, newSocket1, newSocket2;
    int n;
    char buffer[256];
    void game();
    void sendMessage(char* msg, int who);
    char* readMessage(int who);
    int getNumber(string text);
    char getLetter(string text);
public:
    Server(int hostShort);
    ~Server() {
        close(newSocket1);
        close(newSocket2);
        close(mySocket);
    }
};

