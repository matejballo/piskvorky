#include "server/Server.h"
#include "client/Client.h"


int main(int argc, char * argv[]) {
   if (argc > 2) {
        if(atoi(argv[1]) == 0) {
            Server* server = new Server(atoi(argv[2]));
            delete server;
        }
        if(atoi(argv[1]) == 1)  {
            Client* client = new Client(argv[2]);

            client->startCommunication();
            delete client;
        }
    }
    return 0;
}