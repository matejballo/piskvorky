//
// Created by Matej on 30. 11. 2020.
//
#include <iostream>
#include <string>
#include <cctype>
#include "Player.h"

class Game {
private:
    int** array;
    int arraySize;
    Player* player1;
    Player* player2;
    void markOption(int par_x, int par_y, char option);
    void processGameState();
    int getArraySize();

public:
    Game(int par_size, string nick1, string nick2) {
        this->array = new int *[par_size];
        this->arraySize = par_size;
        //Generate parArray
        for(int i = 0; i <par_size; i++) {
            array[i] = new int[par_size];
            for(int j=0; j<par_size; j++) {
                array[i][j]=0;
            }
        }
        this->player1 = new Player(nick1);
        this->player2 = new Player(nick2);
    }
    ~Game() {
        delete player1;
        delete player2;
        for (int i = 0; i < arraySize; ++i) {
            delete[] array[i];
        }
        delete[] array;
    }
    bool processUserInput(int round, int number, char letter);
    char* gameOver();
    char* getScore();
};


