#include "Game.h"
#include <cstring>
#include <sstream>

using namespace std;

void Game::processGameState() {
    int scorePlayer1 = 0;
    int scorePlayer2 = 0;
    int sumDiagonalLeft = 0;
    int sumDiagonalRight = 0;
    for (int i = 0; i < this->getArraySize(); i++) {
        int sumRow = 0;
        int sumColumn = 0;
        for (int j = 0; j < this->getArraySize(); j++) {
            sumRow += this->array[i][j];
            sumColumn += this->array[j][i];
            if (i == j) {
                sumDiagonalLeft += this->array[i][j];
            }
            if ((this->getArraySize() - j - 1) == i) {
                sumDiagonalRight += this->array[i][j];
            }
        }
        if ((sumRow == this->getArraySize()) || (sumColumn == this->getArraySize())) {
            scorePlayer1++;
        }
        if ((sumRow == (this->getArraySize() * (this->getArraySize() + 1))) ||
            (sumColumn == (this->getArraySize() * (this->getArraySize() + 1)))) {
            scorePlayer2++;
        }
    }
    if ((sumDiagonalLeft == this->getArraySize()) || (sumDiagonalRight == this->getArraySize())) {
        scorePlayer1++;
    }
    if ((sumDiagonalLeft == (this->getArraySize() * (this->getArraySize() + 1))) ||
        (sumDiagonalRight == (this->getArraySize() * (this->getArraySize() + 1)))) {
        scorePlayer2++;
    }
    this->player1->setScore(scorePlayer1);
    this->player2->setScore(scorePlayer2);
}

bool Game::processUserInput(int round, int number, char letter) {
    bool incorrectInput = true;
    letter = toupper(letter);

    //Check type of input data
    if (isalpha(letter)>0 && isalpha(number)==0) {
        int x = number - 1;
        int y = (int) letter - 65;
        char user_symbol = 'X';
        if ((round % 2) == 1) {
            user_symbol = 'O';
        }

        //Check user input
        if (x < this->getArraySize() && y < this->getArraySize() && x >= 0 && y >= 0 && (this->array[x][y] == 0)) {
            this->markOption(x, y, user_symbol);

            //Check score
            this->processGameState();
            incorrectInput = false;
        }
    }
    return incorrectInput;
}

void Game::markOption(int par_x, int par_y, char option) {
    option = toupper(option);
    if (option == 'X') {
        array[par_x][par_y] = 1;
    }
    if (option == 'O') {
        array[par_x][par_y] = this->getArraySize() + 1;
    }
}

char *Game::gameOver() {
    ostringstream outStr;
    outStr << "Game over!\n";
    if (this->player1->getScore() == this->player2->getScore()) {
        outStr << "It ended in a draw.\n";
    } else {
        outStr << "Winner: ";
        if (this->player1->getScore() > this->player2->getScore()) {
            outStr << this->player1->getNick();
        } else {
            outStr << this->player2->getNick();
        }
        outStr << "Congratulation!\n";
    }
    char *result = strdup(outStr.str().c_str());
    return result;
}

char *Game::getScore() {
    ostringstream outStr;
    outStr << this->player1->getScore() << " " << this->player2->getScore();
    char *result = strdup(outStr.str().c_str());
    return result;
}

int Game::getArraySize() {
    return this->arraySize;
}