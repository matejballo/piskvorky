//
// Created by Matej on 30. 11. 2020.
//

#include "Player.h"

string Player::getNick() {
    return this->nick;
}

int Player::getScore() {
    return this->score;
}

void Player::setScore(int newScore) {
    this->score = newScore;
}