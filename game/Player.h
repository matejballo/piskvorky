//
// Created by Matej on 30. 11. 2020.
//

#include <iostream>

using namespace std;

class Player {
private:
    string nick;
    int score;

public:
    Player(string par_nick) {
        nick = par_nick;
        score = 0;
    }
    string getNick();
    int getScore();
    void setScore(int newScore);
};

