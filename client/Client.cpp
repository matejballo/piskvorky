//
// Created by Matej on 29. 12. 2020.
//

#include <iostream>
#include "Client.h"

int Client::createSocket() {
    //Create socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Error creating socket");
        return 3;
    }
    return 0;
}

int Client::connectToSocket() {
    //Connect to socket
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("Error connecting to socket");
        return 4;
    }
    return 0;
}

void *Client::funDraw(void *param) {
    DRAWER *drawer = (DRAWER *) param;
    for (int o = 1; o < drawer->sizeOfArray * drawer->sizeOfArray; o++) {
        pthread_mutex_lock(drawer->mutex);
        while (!(*drawer->gridHasChanged)) {
            pthread_cond_wait(drawer->draw, drawer->mutex);
        }
        cout << "   ";
        for (int i = 65; i < (65 + drawer->sizeOfArray); i++) {
            cout << char(i) << "   ";
        }
        cout << endl;
        for (int i = 0; i < drawer->sizeOfArray; i++) {
            string new_line = "  ";
            cout << (i + 1) << " ";
            for (int j = 0; j < drawer->sizeOfArray; j++) {
                string symbol = " ";
                if (drawer->parArray[i*drawer->sizeOfArray+j] == (drawer->sizeOfArray + 1)) {
                    symbol = "O";
                }
                if (drawer->parArray[i*drawer->sizeOfArray+j] == 1) {
                    symbol = "X";
                }
                cout << " " << symbol << " ";
                if (j < (drawer->sizeOfArray - 1)) {
                    cout << "|";
                    new_line += "---+";
                } else {
                    cout << endl;
                    new_line += "---";
                }
            }
            if (i < (drawer->sizeOfArray - 1)) {
                cout << new_line << endl;
            }
        }
        *drawer->gridHasChanged = false;
        pthread_mutex_unlock(drawer->mutex);
    }
    return nullptr;
}

int Client::startCommunication() {
    //Invitation
    readMessageFromServer();
    printf("%s", buffer);

    //Enter nick
    writeMessageToServer();

    //Order of players and size of grid
    readMessageFromServer();

    int gridSize = buffer[2] - '0';
    array = (int *) malloc(gridSize * gridSize * sizeof(int));
    printf("Size of playing area is: %dx%d\n", gridSize, gridSize);

    DRAWER parDrawer = {gridSize, &gridHasChanged, array, &mutex, &drawCond};
    pthread_create(&drawer_thread, nullptr, &funDraw, &parDrawer);

    int replications = gridSize * gridSize;
    if (buffer[0] == '1') {
        firstPlayer = true;
        if ((gridSize % 2) == 0) {
            replications /= 2;
        } else {
            replications = (replications - 1) / 2 + 1;
        }
        printf("%s \n", buffer + 5);
    } else {
        firstPlayer = false;
        if ((gridSize % 2) == 0) {
            replications /= 2;
        } else {
            replications = (replications - 1) / 2;
        }
    }

    for (int i = 0; i < replications; i++) {
        if (firstPlayer) {
            //Move
            bool input = false;
            char letter, number;
            while (!input) {
                writeMessageToServer();
                number = buffer[0];
                letter = buffer[2];
                readMessageFromServer(2);
                if ((buffer[0] - '0') == 0) {
                    input = true;
                } else {
                    printf("Your input was incorrect! Try again!\n");
                }
            }
            system("clear");
            pthread_mutex_lock(&mutex);
            gridHasChanged = true;
            this->array[(number - '0' - 1) * gridSize + ((int) letter - 65)] = 1;
            pthread_cond_signal(&drawCond);
            pthread_mutex_unlock(&mutex);

            //Loading score
            this->readMessageFromServer(4);
            printf("SCORE: me -> %s <- opponent\n",buffer);

            if (i != (replications - 1)) {
                //Loading move of opponent
                this->readMessageFromServer(5);

                system("clear");
                //Changing grid
                pthread_mutex_lock(&mutex);
                gridHasChanged = true;
                this->array[(buffer[0] - '0' - 1) * gridSize + ((int) buffer[2] - 65)] = (gridSize + 1);
                pthread_mutex_unlock(&mutex);
                pthread_cond_signal(&drawCond);
                //Loading score
                this->readMessageFromServer(4);
                printf("SCORE: me -> %s <- opponent\n", buffer);

                //Message for player to make move
                this->readMessageFromServer(44);
                printf("%s\n", buffer);
            }
        } else {
            //Loading move of opponent
            this->readMessageFromServer(5);

            system("clear");
            //Changing grid
            pthread_mutex_lock(&mutex);
            gridHasChanged = true;
            this->array[(buffer[0] - '0' - 1) * gridSize + ((int) buffer[2] - 65)] = 1;
            pthread_cond_signal(&drawCond);
            pthread_mutex_unlock(&mutex);

            //Loading score
            this->readMessageFromServer(4);
            printf("SCORE: opponent -> %s <- me\n", buffer);

            //Message for player to make move
            this->readMessageFromServer(44);
            printf("%s \n", buffer);

            //Move
            bool input = false;
            char letter, number;
            while (!input) {
                writeMessageToServer();
                number = buffer[0];
                letter = buffer[2];
                readMessageFromServer(2);
                if ((buffer[0] - '0') == 0) {
                    input = true;
                } else {
                    printf("Your input was incorrect! Try again!\n");
                }
            }

            system("clear");
            //Changing grid
            pthread_mutex_lock(&mutex);
            gridHasChanged = true;
            this->array[(number - '0' - 1) * gridSize + ((int) letter - 65)] = (gridSize + 1);
            pthread_mutex_unlock(&mutex);
            pthread_cond_signal(&drawCond);

            //Loading score
            this->readMessageFromServer(4);
            printf("SCORE: opponent -> %s <- me\n", buffer);
        }
    }
    if(!firstPlayer && ((gridSize % 2) == 1)) {
        this->readMessageFromServer(5);
        this->readMessageFromServer(4);
    }
    if(firstPlayer && ((gridSize % 2) == 0)) {
        this->readMessageFromServer(5);
        this->readMessageFromServer(4);
    }

    //Evaluation
    this->readMessageFromServer();
    printf("%s",buffer);
    pthread_join(drawer_thread, nullptr);
    close(sockfd);
    return 0;
}