#pragma once
//
// Created by Matej on 29. 12. 2020.

#include <cstdio>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <cstdio>
#include <pthread.h>
#include <unistd.h>
#include <stdexcept>

using namespace std;

class Client {

    typedef struct drawer {
         int sizeOfArray;
         volatile bool * gridHasChanged;
         volatile int * parArray;
         pthread_mutex_t * mutex;
         pthread_cond_t * draw;
    } DRAWER;

private:
    int sockfd, n;
    bool gridHasChanged = false;
    struct sockaddr_in serv_addr;
    struct hostent* server;
    char buffer[256];
    int * array;
    bool firstPlayer;

    pthread_t drawer_thread;
    pthread_cond_t drawCond;
    pthread_mutex_t mutex;

    int createSocket();
    int connectToSocket();

    void writeMessageToServer() {
        bzero(buffer, 256);
        fgets(buffer, 255, stdin);
        n = write(sockfd, buffer, strlen(buffer));
        if (n < 0) {
            perror("Error writing to socket");
        }
    }

    void readMessageFromServer() {
        bzero(buffer, 256);
        n = read(sockfd, buffer, 255);
        if (n < 0) {
            perror("Error reading from socket");
        }
    }

    void readMessageFromServer(size_t count) {
        bzero(buffer, 256);
        n = read(sockfd, buffer, count);
        if (n < 0) {
            perror("Error reading from socket");
        }
    }
    static void * funDraw(void * param);

public:
    Client(char* hostShort) {
        server = gethostbyname("localhost");
        bzero((char*)&serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy(
                (char*)server->h_addr,
                (char*)&serv_addr.sin_addr.s_addr,
                server->h_length
        );
        serv_addr.sin_port = htons(atoi(hostShort));

        mutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_cond_init(&drawCond, nullptr);

        this->createSocket();
        this->connectToSocket();
    }

    int startCommunication();

    ~Client() {
        pthread_cond_destroy(&drawCond);
        pthread_mutex_destroy(&mutex);
        free(array);
    }
};